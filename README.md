# Scripts to determine analyzable regions in the genome
**analyze_5ends.sh**

This script generates a genome mask indicating which nucleotides (5' ends) can be used for analysis. This evaluation is based on a evaluation of the mappability of this individual nucleotide.
The rules for good positions are:
  uniq 20mer (defined by SHORT) starting at this position 
  multi-mapping 20mers at this position that have the same mapping-number as a 35mer (defined by LONG) starting at the exact same position

This evaluation is performed for the whole genome on both strands. The results are statistics about the mappability of the genome and a mask-file in bed format that can be easily intersected with other bed-files to filter for analyzable 5'ends.

## Used tools:
bedToBigBed/2.7

bowtie/0.12.9

samtools/0.1.1

bamtools/2.1.1

bedtools/2.19.1

optional if you run on cluster

gridengine/2011.11

## Input:
The Input can be any genome of interest. For this genome the following files must exist:
 
  **genome-fasta**        file should contain all chromosomes in fasta format
 
  **bowtie-index**        bowtie index generated from the genome-fasta file
 
  **chromosome-sizes**    tab separated file in the format: chromosome-name length-of-chromosome

## Variables which need to be set in create_tables.sh: 

**COMPUTING**= specification if the script should run
    locally (L; slow as processes one chromosome after the other)
    cluster (C; uses gridengine; fast as chromoosomal processing occurs in parallel; requires the setting of a proper sungrid-header and the change of the host-name in the block marked with #@@@@@@@ in the run_analysis.sh file )
              
**NAME**= Analysis-name used for the naming of all folders

**FOLDER**= Path to permanently store final results

**TMP**=  Path for the temporary storage of intermediate files

**PUBLIC**= folder accessible to the web to have easy access to statistics- and log-files
                does not need to be web-open

**GENOME_FASTA**= full path to the file containing all chromosomes in fasta format

**BOWTIE_INDEX**= full path to the bowtie index (omit everything after .ebwt)

**CHR_SIZE**= full path to the file containing the chromsome sizes 

**SHORT**= size of the shortest fragment you would like to analyze

**LONG**= size of the longest fragment you would like to analyze


## Output

It will generate the following output files:

### into the public directory:

**analyzable_regions.bb** - track for UCSC to visualize the analyzable regions

**analyzable_regions_uniq_only.bb** - track for UCSC to visualize the analyzable regions - restricted to unique mapping 20mers only

**log.txt** - various informations about the run

**mappability_1kb-tiles.txt** - 1kb tiles for each chromosome with the % mappability reported

**mappability_full_chromosome.txt** - % mappability of each chromosome	

**time.log**  - time-statistics for the individual processing steps

### into the permanent directory:

**analyzable_regions.bed** - bed-file containing the good regions as intervals ready for intersection with other bed-files

**analyzable_regions_uniq_only.bed** - bed-file containing the uniq-only regions as intervals ready for intersection with other bed-files
