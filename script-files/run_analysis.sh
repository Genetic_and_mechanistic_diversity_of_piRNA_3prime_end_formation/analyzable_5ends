#$ -S /bin/sh

#enter sungrid-header


set -u
TIME_ORIG=$(date "+%s")

###################################################################################################

#extract variables if computing on PIWI

vari=

while getopts Òhv:Ó OPTION
do
  case $OPTION in
    h)
      usage
      exit 1
      ;;
    v)
      vari=$OPTARG
      ;;      
    ?)
      usage
      exit
      ;;
  esac
done

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#check if host is piwi and process variables accordingly
##chnge the hostname from compute- to the name you cluster nodes have
##this block is only important for local processing to preset the variables
host=$(uname -n)
if [[ ! "$host" =~ "compute-" ]]
then
  NSLOTS=10
  vari=$(echo "$vari" | sed 's/,/\t/g;s/"//g'  )
  eval "$vari"#$ -S /bin/sh

#enter sungrid-header


set -u
TIME_ORIG=$(date "+%s")

###################################################################################################

#extract variables if computing on PIWI

vari=

while getopts Òhv:Ó OPTION
do
  case $OPTION in
    h)
      usage
      exit 1
      ;;
    v)
      vari=$OPTARG
      ;;      
    ?)
      usage
      exit
      ;;
  esac
done

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#check if host is piwi and process variables accordingly
##chnge the hostname from compute- to the name you cluster nodes have
##this block is only important for local processing to preset the variables
host=$(uname -n)
if [[ ! "$host" =~ "compute-" ]]
then
  NSLOTS=10
  vari=$(echo "$vari" | sed 's/,/\t/g;s/"//g'  )
  eval "$vari"
fi 
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###################################################################################################
#extract current chromsome info
CHROM=$(sed -n "${SGE_TASK_ID}"p "${CHR_SIZE}"  | tr ' ' '\t' | cut -f 1 )
SIZE=$(sed -n "${SGE_TASK_ID}"p "${CHR_SIZE}" | tr ' ' '\t' | cut -f 2 )


cat "${CHR_SIZE}"
CHROM="${CHROM//chr/}" 

##create chromosome specific directories
TMP="${XTMP}/${CHROM}/"
PUBLIC="${PUBLIC}/time_logs/"
FOLDER="${FOLDER}/${CHROM}/"

rm -rf ${TMP}/*
rm -rf ${PUBLIC}/*
rm -rf ${FOLDER}/* 

mkdir -p "$TMP"
mkdir -p "$PUBLIC"
mkdir -p "$FOLDER"

###################################################################################################
##generate bed index for chromosome

TIME=$(date "+%s")

#--------------------------------------------------------------------------------------------------
#split chromosomal sequence into SMALL bits
module load bedtools/2.19.1
echo "$CHROM $SIZE" |\
  awk -v OFS="\t" -v SHORT="$SHORT" -v LONG="$LONG" -v STRAND="$STRAND" '{ 
  if( STRAND=="+") {
    for(i=0;i<=$2-LONG;i++) print $1,i,i+SHORT,$1"~"i,".","+"
  } else {
    for(i=$2;i>=LONG;i--) print $1,i-SHORT,i,$1"~"i,".","-"    
  }
  }' | \
  bedtools getfasta -s -tab -name -bed - -fi "$GENOME_FASTA" -fo - | \
  awk '{if($2!~"N") print ">"$1"\n"$2 }' > "${TMP}${CHROM}_${SHORT}_${STRAND}.fa"
  
TIME2=$(date "+%s")
echo time for generating fasta from chromosome $(( ( TIME2 - TIME ) / 60 )) min $(( ( TIME2 - TIME ) % 60 )) sec > "${PUBLIC}time_log_${CHROM}_${STRAND}.txt"
TIME="$TIME2"
  
#-------------------------------------------------------   -------------------------------------------
#map reads to the genome - unique mappings only
module load bowtie/0.12.9
module load samtools/0.1.18
module load bamtools/2.1.1
CORES=$(( NSLOTS - 3 ))
bowtie -f -S -p "$CORES" -v 0 -m 1 --un "${TMP}${CHROM}_non-uniq_${STRAND}.fa" "$BOWTIE_INDEX" "${TMP}${CHROM}_${SHORT}_${STRAND}.fa" | \
  samtools view -bS - | \
  bamToBed -i - | \
  awk -v OFS="\t" -v LONG="$LONG" -v STRAND="$STRAND" '
    {if(STRAND=="+") {
        $3=$2+LONG; $4=$4"@u"; print 
      } else {
        $2=$3-LONG; $4=$4"@u"; print 
      }
    }' > "${TMP}${CHROM}_fin_${STRAND}.bed"

TIME2=$(date "+%s")
echo time for mapping unique reads $(( ( TIME2 - TIME ) / 60 )) min $(( ( TIME2 - TIME ) % 60 )) sec >> "${PUBLIC}time_log_${CHROM}_${STRAND}.txt"
TIME="$TIME2"
  
#--------------------------------------------------------------------------------------------------
#get long version of the multi-mapping short-reads
cat "${TMP}${CHROM}_non-uniq_${STRAND}.fa" | awk '/^>/&&NR>1{print "";}{ printf "%s",/^>/ ? $0" ":$0 } END{printf "\n"} ' | \
  sed 's/>//' > "${TMP}${CHROM}_non-uniq_${STRAND}.fa.tmp" 
  
cat "${TMP}${CHROM}_non-uniq_${STRAND}.fa" | \
  awk -v OFS="\t" -v LONG="$LONG" -v STRAND="$STRAND" '{ 
    if($1~">") {
      gsub(/>/, ""); split($1,X,/~/);
    if(STRAND=="+") {
        print X[1],X[2],X[2]+LONG,$1"_"LONG,".","+" 
      }else{
        print X[1],X[2]-LONG,X[2],$1"_"LONG,".","-"       
      }
    }
  }' | \
  bedtools getfasta -s -tab -name -bed - -fi "$GENOME_FASTA" -fo - >> "${TMP}${CHROM}_non-uniq_${STRAND}.fa.tmp"
  
TIME2=$(date "+%s")
echo time getting 40mer sequences into fasta file $(( ( TIME2 - TIME ) / 60 )) min $(( ( TIME2 - TIME ) % 60 )) sec >> "${PUBLIC}time_log_${CHROM}_${STRAND}.txt"
TIME="$TIME2"

#--------------------------------------------------------------------------------------------------
#collapse long version of multi-mapping shoirt-reads to individual sequences
CORES=$(( NSLOTS - 1 ))
sort -k2,2 --parallel "$CORES" "${TMP}${CHROM}_non-uniq_${STRAND}.fa.tmp" |\
  awk -v TMP="$TMP" -v STRAND="$STRAND" 'BEGIN{ NUM=1; LOCATION=TMP"IDs_"STRAND".txt" } 
        {
          if(NR==1) {X=$2;Y=$1"!";} 
          else {
            if($2==X){ 
              Y=Y$1"!";
            }else{
              gsub(/!$/,"",Y); print "ID_"NUM,Y > LOCATION; print ">ID_"NUM"\n"X; X=$2; Y=$1"!"; NUM=NUM+1;
            }
          }
        }
        END {gsub(/!$/,"",Y); print "ID_"NUM,Y > LOCATION; print ">ID_"NUM"\n"X }' > "${TMP}${CHROM}_non-uniq_${STRAND}.fa.coll"
        
TIME2=$(date "+%s")
echo time for collapsing the fasta file $(( ( TIME2 - TIME ) / 60 )) min $(( ( TIME2 - TIME ) % 60 )) sec >> "${PUBLIC}time_log_${CHROM}_${STRAND}.txt"
TIME="$TIME2"
       
#--------------------------------------------------------------------------------------------------
#map uniquely unmapped reads to genome - allow all-mappers

CORES=$(( NSLOTS - 3 ))
bowtie -f -S -p "$CORES" -v 0 -a "$BOWTIE_INDEX" "${TMP}${CHROM}_non-uniq_${STRAND}.fa.coll" | \
  samtools view -bS - | \
  bamToBed -i - | \
  cut -f 4 | \
  awk -v OFS="\t" -v LONG="$LONG"  -v TMP="$TMP" -v STRAND="$STRAND" '
  #read ID file into array
  #L=Location; I=ID; A=Array
  BEGIN {L=TMP"IDs_"STRAND".txt"; while((getline I < L) > 0) {split(I,J,/ /); A[J[1]]=J[2] }}
  { 
    #split header into individual entries and run lookup for all
    N=split(A[$1],NAMES,/!/); for(i=1; i<=N; i++) { 
      if (NAMES[i] ~ "_"LONG) { 
        split(NAMES[i],NAME_LONG,/_/); X[NAME_LONG[1]]+=1
      } else { 
        Y[NAMES[i]]+=1
      }
    }
  }
  END{for (i in X)
    { if(X[i]==Y[i]) {
        if (STRAND=="+") {
          split(i,Z,/~/); print Z[1],Z[2],Z[2]+LONG,i"@m",".","+"
        }else{
          split(i,Z,/~/); print Z[1],Z[2]-LONG,Z[2],i"@m",".","-"
        } 
      }
    }
  }  ' >> "${TMP}${CHROM}_fin_${STRAND}.bed"
  

TIME2=$(date "+%s")
echo time for mapping and filtering multimapper 20mers and 40mers $(( ( TIME2 - TIME ) / 60 )) min $(( ( TIME2 - TIME ) % 60 )) sec >> "${PUBLIC}time_log_${CHROM}_${STRAND}.txt"
TIME="$TIME2"

#--------------------------------------------------------------------------------------------------
#convert bed-file into bedgraph

module load bedtools/2.19.1
CORES=$(( NSLOTS - 1 )) 
sort -k1,1 -k2,2n --parallel="$CORES" "${TMP}${CHROM}_fin_${STRAND}.bed" | \
  awk -v OFS="\t" -v STRAND="$STRAND" '{
    if(STRAND=="+") {
      $1="chr"$1; $3=$2+1 
    }else{
      $1="chr"$1; $2=$3-1 
    }
  print }' > "${TMP}sort_${STRAND}.bed"  
head "$CHR_SIZE"
genomeCoverageBed -bg -i "${TMP}sort_${STRAND}.bed" -g "$CHR_SIZE" > "${TMP}${CHROM}_${STRAND}.bedgraph"

 
  awk -v OFS="\t" '{if($4~"@u") print $0}' < "${TMP}sort_${STRAND}.bed" > "${TMP}uniq_${STRAND}.bed"   
genomeCoverageBed -bg -i "${TMP}uniq_${STRAND}.bed"  -g "$CHR_SIZE" > "${TMP}${CHROM}_uniq_${STRAND}.bedgraph"

TIME2=$(date "+%s")
echo time to generate bedgraph files $(( ( TIME2 - TIME ) / 60 )) min $(( ( TIME2 - TIME ) % 60 )) sec >> "${PUBLIC}time_log_${CHROM}_${STRAND}.txt"
TIME="$TIME2"


fi 
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###################################################################################################
#extract current chromsome info
CHROM=$(sed -n "${SGE_TASK_ID}"p "${CHR_SIZE}"  | tr ' ' '\t' | cut -f 1 )
SIZE=$(sed -n "${SGE_TASK_ID}"p "${CHR_SIZE}" | tr ' ' '\t' | cut -f 2 )


cat "${CHR_SIZE}"
CHROM="${CHROM//chr/}" 

##create chromosome specific directories
TMP="${XTMP}/${CHROM}/"
PUBLIC="${PUBLIC}/time_logs/"
FOLDER="${FOLDER}/${CHROM}/"

rm -rf ${TMP}/*
rm -rf ${PUBLIC}/*
rm -rf ${FOLDER}/* 

mkdir -p "$TMP"
mkdir -p "$PUBLIC"
mkdir -p "$FOLDER"

###################################################################################################
##generate bed index for chromosome

TIME=$(date "+%s")

#--------------------------------------------------------------------------------------------------
#split chromosomal sequence into SMALL bits
module load bedtools/2.19.1
echo "$CHROM $SIZE" |\
  awk -v OFS="\t" -v SHORT="$SHORT" -v LONG="$LONG" -v STRAND="$STRAND" '{ 
  if( STRAND=="+") {
    for(i=0;i<=$2-LONG;i++) print $1,i,i+SHORT,$1"~"i,".","+"
  } else {
    for(i=$2;i>=LONG;i--) print $1,i-SHORT,i,$1"~"i,".","-"    
  }
  }' | \
  bedtools getfasta -s -tab -name -bed - -fi "$GENOME_FASTA" -fo - | \
  awk '{if($2!~"N") print ">"$1"\n"$2 }' > "${TMP}${CHROM}_${SHORT}_${STRAND}.fa"
  
TIME2=$(date "+%s")
echo time for generating fasta from chromosome $(( ( TIME2 - TIME ) / 60 )) min $(( ( TIME2 - TIME ) % 60 )) sec > "${PUBLIC}time_log_${CHROM}_${STRAND}.txt"
TIME="$TIME2"
  
#-------------------------------------------------------   -------------------------------------------
#map reads to the genome - unique mappings only
module load bowtie/0.12.9
module load samtools/0.1.18
module load bamtools/2.1.1
CORES=$(( NSLOTS - 3 ))
bowtie -f -S -p "$CORES" -v 0 -m 1 --un "${TMP}${CHROM}_non-uniq_${STRAND}.fa" "$BOWTIE_INDEX" "${TMP}${CHROM}_${SHORT}_${STRAND}.fa" | \
  samtools view -bS - | \
  bamToBed -i - | \
  awk -v OFS="\t" -v LONG="$LONG" -v STRAND="$STRAND" '
    {if(STRAND=="+") {
        $3=$2+LONG; $4=$4"@u"; print 
      } else {
        $2=$3-LONG; $4=$4"@u"; print 
      }
    }' > "${TMP}${CHROM}_fin_${STRAND}.bed"

TIME2=$(date "+%s")
echo time for mapping unique reads $(( ( TIME2 - TIME ) / 60 )) min $(( ( TIME2 - TIME ) % 60 )) sec >> "${PUBLIC}time_log_${CHROM}_${STRAND}.txt"
TIME="$TIME2"
  
#--------------------------------------------------------------------------------------------------
#get long version of the multi-mapping short-reads
cat "${TMP}${CHROM}_non-uniq_${STRAND}.fa" | awk '/^>/&&NR>1{print "";}{ printf "%s",/^>/ ? $0" ":$0 } END{printf "\n"} ' | \
  sed 's/>//' > "${TMP}${CHROM}_non-uniq_${STRAND}.fa.tmp" 
  
cat "${TMP}${CHROM}_non-uniq_${STRAND}.fa" | \
  awk -v OFS="\t" -v LONG="$LONG" -v STRAND="$STRAND" '{ 
    if($1~">") {
      gsub(/>/, ""); split($1,X,/~/);
    if(STRAND=="+") {
        print X[1],X[2],X[2]+LONG,$1"_"LONG,".","+" 
      }else{
        print X[1],X[2]-LONG,X[2],$1"_"LONG,".","-"       
      }
    }
  }' | \
  bedtools getfasta -s -tab -name -bed - -fi "$GENOME_FASTA" -fo - >> "${TMP}${CHROM}_non-uniq_${STRAND}.fa.tmp"
  
TIME2=$(date "+%s")
echo time getting 40mer sequences into fasta file $(( ( TIME2 - TIME ) / 60 )) min $(( ( TIME2 - TIME ) % 60 )) sec >> "${PUBLIC}time_log_${CHROM}_${STRAND}.txt"
TIME="$TIME2"

#--------------------------------------------------------------------------------------------------
#collapse long version of multi-mapping shoirt-reads to individual sequences
CORES=$(( NSLOTS - 1 ))
sort -k2,2 --parallel "$CORES" "${TMP}${CHROM}_non-uniq_${STRAND}.fa.tmp" |\
  awk -v TMP="$TMP" -v STRAND="$STRAND" 'BEGIN{ NUM=1; LOCATION=TMP"IDs_"STRAND".txt" } 
        {
          if(NR==1) {X=$2;Y=$1"!";} 
          else {
            if($2==X){ 
              Y=Y$1"!";
            }else{
              gsub(/!$/,"",Y); print "ID_"NUM,Y > LOCATION; print ">ID_"NUM"\n"X; X=$2; Y=$1"!"; NUM=NUM+1;
            }
          }
        }
        END {gsub(/!$/,"",Y); print "ID_"NUM,Y > LOCATION; print ">ID_"NUM"\n"X }' > "${TMP}${CHROM}_non-uniq_${STRAND}.fa.coll"
        
TIME2=$(date "+%s")
echo time for collapsing the fasta file $(( ( TIME2 - TIME ) / 60 )) min $(( ( TIME2 - TIME ) % 60 )) sec >> "${PUBLIC}time_log_${CHROM}_${STRAND}.txt"
TIME="$TIME2"
       
#--------------------------------------------------------------------------------------------------
#map uniquely unmapped reads to genome - allow all-mappers

CORES=$(( NSLOTS - 3 ))
bowtie -f -S -p "$CORES" -v 0 -a "$BOWTIE_INDEX" "${TMP}${CHROM}_non-uniq_${STRAND}.fa.coll" | \
  samtools view -bS - | \
  bamToBed -i - | \
  cut -f 4 | \
  awk -v OFS="\t" -v LONG="$LONG"  -v TMP="$TMP" -v STRAND="$STRAND" '
  #read ID file into array
  #L=Location; I=ID; A=Array
  BEGIN {L=TMP"IDs_"STRAND".txt"; while((getline I < L) > 0) {split(I,J,/ /); A[J[1]]=J[2] }}
  { 
    #split header into individual entries and run lookup for all
    N=split(A[$1],NAMES,/!/); for(i=1; i<=N; i++) { 
      if (NAMES[i] ~ "_"LONG) { 
        split(NAMES[i],NAME_LONG,/_/); X[NAME_LONG[1]]+=1
      } else { 
        Y[NAMES[i]]+=1
      }
    }
  }
  END{for (i in X)
    { if(X[i]==Y[i]) {
        if (STRAND=="+") {
          split(i,Z,/~/); print Z[1],Z[2],Z[2]+LONG,i"@m",".","+"
        }else{
          split(i,Z,/~/); print Z[1],Z[2]-LONG,Z[2],i"@m",".","-"
        } 
      }
    }
  }  ' >> "${TMP}${CHROM}_fin_${STRAND}.bed"
  

TIME2=$(date "+%s")
echo time for mapping and filtering multimapper 20mers and 40mers $(( ( TIME2 - TIME ) / 60 )) min $(( ( TIME2 - TIME ) % 60 )) sec >> "${PUBLIC}time_log_${CHROM}_${STRAND}.txt"
TIME="$TIME2"

#--------------------------------------------------------------------------------------------------
#convert bed-file into bedgraph

module load bedtools/2.19.1
CORES=$(( NSLOTS - 1 )) 
sort -k1,1 -k2,2n --parallel="$CORES" "${TMP}${CHROM}_fin_${STRAND}.bed" | \
  awk -v OFS="\t" -v STRAND="$STRAND" '{
    if(STRAND=="+") {
      $1="chr"$1; $3=$2+1 
    }else{
      $1="chr"$1; $2=$3-1 
    }
  print }' > "${TMP}sort_${STRAND}.bed"  
head "$CHR_SIZE"
genomeCoverageBed -bg -i "${TMP}sort_${STRAND}.bed" -g "$CHR_SIZE" > "${TMP}${CHROM}_${STRAND}.bedgraph"

 
  awk -v OFS="\t" '{if($4~"@u") print $0}' < "${TMP}sort_${STRAND}.bed" > "${TMP}uniq_${STRAND}.bed"   
genomeCoverageBed -bg -i "${TMP}uniq_${STRAND}.bed"  -g "$CHR_SIZE" > "${TMP}${CHROM}_uniq_${STRAND}.bedgraph"

TIME2=$(date "+%s")
echo time to generate bedgraph files $(( ( TIME2 - TIME ) / 60 )) min $(( ( TIME2 - TIME ) % 60 )) sec >> "${PUBLIC}time_log_${CHROM}_${STRAND}.txt"
TIME="$TIME2"

