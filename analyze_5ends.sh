 set -u
  
###################################################################################################

#preset software
##these tools have to exist in you path variable
#bedToBigBed/2.7
#bowtie/0.12.9
#samtools/0.1.1
#bamtools/2.1.1
#bedtools/2.19.1

#optional if you run on cluster
#gridengine/2011.11

###################################################################################################


#preset all variables
#computing on cluster [C] or local processing [L]
#cluster computing is running on SungridEngine
#for cluster computing the submission further down and the header in the sub-script needs to get changed
COMPUTING=L

#run name
NAME=

#folder for permanent storage of results
FOLDER=
#temprary processing folder
TMP=
#folder accessible by web to get easy access (does not need to be accessible by web)
#final tables will be deposited into this folder
PUBLIC=

#path to genome fasta-file
GENOME_FASTA=
#path to bowtie-index
BOWTIE_INDEX=
#path to chromosome-size file
CHR_SIZE=


#define short and long read length for end-analysis
SHORT=20
LONG=35


###################################################################################################
#presetting of path-variables
FOLDER="${FOLDER}${NAME}/"
TMP="${TMP}${NAME}/"
PUBLIC="${PUBLIC}${NAME}/"
PUBLIC_CORE=$(echo "$PUBLIC" | sed 's/tmp\//\t/' | cut -f 2 )


mkdir -p "$FOLDER"
mkdir -p "$TMP"
mkdir -p "$PUBLIC"

TIME=$(date "+%s")
TIME_ORIG=$(date "+%s")

#--------------------------------------------------------------------------------------------------
#determine the location of the top script
##set script-dir automatically

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
SCRIPT_FOLDER="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
SCRIPT_FOLDER="${SCRIPT_DIRraw}/"
SCRIPT_FOLDER="${SCRIPT_DIRraw}script-files/"


###################################################################################################
#preset variables for local or cluster-computing
if [[ "$COMPUTING" == C ]]
then
  subc="qsub -sync y"
  subc2="qsub "
  quote=""
  BG="&"
  STRANDS_AVAIL="+ -"
    
else
  subc=""
  subc2="$subc"
  quote="\""
  COMPUTING=P
  STRANDS_AVAIL="-"
fi

###################################################################################################
#print details to log files

printf "$NAME\n\nfile-links\n" > "${PUBLIC}log.txt"

echo bed-files: >> "${PUBLIC}log.txt"
echo "${FOLDER}analyzable_regions.bed" >> "${PUBLIC}log.txt"
echo "${FOLDER}analyzable_regions_uniq-only.bed" >> "${PUBLIC}log.txt"
printf "\n\n" >> "${PUBLIC}log.txt"

printf "\nbed-tracks of analyzable 5'ends\n" >> "${PUBLIC}log.txt"
echo -e "track type=bigBed name=\"analyzable_5ends-${NAME}\"  bigDataUrl=http://brenneckelab.imba.oeaw.ac.at/tmp/${PUBLIC_CORE}/analyzable_regions.bb visibility=4 colorByStrand=\"255,0,0 0,0,255\" maxItems=10000"  >> "${PUBLIC}log.txt"
echo -e "track type=bigBed name=\"analyzable_5ends-${NAME}_uniq-only\"  bigDataUrl=http://brenneckelab.imba.oeaw.ac.at/tmp/${PUBLIC_CORE}/analyzable_regions_uniq_only.bb visibility=4 colorByStrand=\"255,0,0 0,0,255\" maxItems=10000"  >> "${PUBLIC}log.txt"

printf "\n\n\n\n"  >> "${PUBLIC}log.txt"


echo bowtie-index: "$BOWTIE_INDEX" >> "${PUBLIC}log.txt"
echo genome-fasta: "$GENOME_FASTA" >> "${PUBLIC}log.txt"
echo chromosome-size: "$CHR_SIZE" >> "${PUBLIC}log.txt"
printf "\n" >> "${PUBLIC}log.txt"

#chromosomes analyzed
TEXT=$(cut -f 1 "$CHR_SIZE" | sed 's/chr//' | sort | tr '\n' ' ' )
printf "Chromosomes analyzed: \n$TEXT \n\n" >> "${PUBLIC}log.txt"

TEXT=$(bowtie-inspect -n "$BOWTIE_INDEX" | tr ' ' '\t' | cut -f 1 | sed 's/chr//' | sort | tr '\n' ' ' )
printf "Chromosomes in bowtie index for mapping: \n$TEXT \n\n\n\n\n" >> "${PUBLIC}log.txt"

###################################################################################################
#submit analysis for each chromosome individually

command="${SCRIPT_FOLDER}run_analysis.sh"
nCHROM=$(cat "${CHR_SIZE}" | wc -l )


#analyze + & - strand seperately (on cluster paralell analysis)
for STRAND in $STRANDS_AVAIL
do
  {
  #analyse each chromosome individually (on cluster paralell analysis)
  if [[ "$COMPUTING" == L ]]
  then
    for currN in $(seq 1 "${nCHROM}" )
    do
      {
      vari="SGE_TASK_ID=${currN},FOLDER=${FOLDER},XTMP=${TMP},PUBLIC=${PUBLIC},GENOME_FASTA=${GENOME_FASTA},BOWTIE_INDEX=${BOWTIE_INDEX},CHR_SIZE=${CHR_SIZE},SHORT=${SHORT},LONG=${LONG},STRAND=$STRAND"
      $subc $command -v ${quote}${vari}${quote}
      } 
    done
  else
    vari="FOLDER=${FOLDER},XTMP=${TMP},PUBLIC=${PUBLIC},GENOME_FASTA=${GENOME_FASTA},BOWTIE_INDEX=${BOWTIE_INDEX},CHR_SIZE=${CHR_SIZE},SHORT=${SHORT},LONG=${LONG},STRAND=$STRAND"
    $subc -t 1:"${nCHROM}" -v ${quote}${vari}${quote} $command
  fi
  } &
done
wait    


TIME2=$(date "+%s")
echo summed time for cluster computing $(( ( TIME2 - TIME ) / 60 )) min $(( ( TIME2 - TIME ) % 60 )) sec > "${PUBLIC}time.log"
TIME="$TIME2"

####################################################################################################
#collect data and generate final files

#--------------------------------------------------------------------------------------------------
#clean files
rm -rf "${FOLDER:?}"/*
rm -rf "${TMP}mapable_regions.bed"
rm -rf "${TMP}mapable_regions_uniq.bed"
#--------------------------------------------------------------------------------------------------
#merge files

for STRAND in $STRANDS_AVAIL
do
	while read CHROM SIZE
	do
		echo "$CHROM" "$STRAND"
	  #create all variables to submit
	  CHROM=$(echo "$CHROM" | sed 's/chr//g' )
	  
	  cat "${TMP}${CHROM}/${CHROM}_$STRAND.bedgraph" | \
	    awk -v OFS="\t" -v STRAND="$STRAND" '{ print $1,$2,$3,$1"_"$2,255,STRAND }' >> "${TMP}mapable_regions.bed"
	
	  cat "${TMP}${CHROM}/${CHROM}_uniq_$STRAND.bedgraph" | \
	    awk -v OFS="\t" -v STRAND="$STRAND" '{ print $1,$2,$3,$1"_"$2,255,STRAND }' >> "${TMP}mapable_regions_uniq.bed"
	  
	  
	done < "${CHR_SIZE}"
done

TIME2=$(date "+%s")
echo time for concatenation of results for all chromosomes $(( ( TIME2 - TIME ) / 60 )) min $(( ( TIME2 - TIME ) % 60 )) sec >> "${PUBLIC}time.log"
TIME="$TIME2"

#--------------------------------------------------------------------------------------------------
#sort mapable_regions.bed
sort -k1,1 -k2,2n --parallel 10 "${TMP}mapable_regions.bed" > "${FOLDER}analyzable_regions.bed"
sort -k1,1 -k2,2n --parallel 10 "${TMP}mapable_regions_uniq.bed" > "${FOLDER}analyzable_regions_uniq_only.bed"


TIME2=$(date "+%s")
echo time to sort bed-files $(( ( TIME2 - TIME ) / 60 )) min $(( ( TIME2 - TIME ) % 60 )) sec >> "${PUBLIC}time.log"
TIME="$TIME2"


####################################################################################################
#calculate statistics

#--------------------------------------------------------------------------------------------------
#generate bigbed files 
$bedToBigBed -tab "${FOLDER}analyzable_regions.bed" "$CHR_SIZE" "${PUBLIC}analyzable_regions.bb"
$bedToBigBed -tab "${FOLDER}analyzable_regions_uniq_only.bed" "$CHR_SIZE" "${PUBLIC}analyzable_regions_uniq_only.bb"

#--------------------------------------------------------------------------------------------------
#calculate percent mapability on full chromosomes
cat ${FOLDER}analyzable_regions.bed | \
	awk -v OFS="\t" -v CHR_SIZE="$CHR_SIZE" -v TMP="$TMP" '
	BEGIN { while((getline I < CHR_SIZE ) > 0) {split(I,J,/\t/); A[J[1]]=J[2] }}
	{
		if($6=="+")	X[$1]+=$3-$2
		else Y[$1]+=$3-$2
	}
	END {for(i in X) print i,X[i]*100/A[i],Y[i]*100/A[i] }
	' | sort -k1,1 > "${TMP}all.txt"

cat "${FOLDER}analyzable_regions_uniq_only.bed" | \
	awk -v OFS="\t" -v CHR_SIZE="$CHR_SIZE" -v TMP="$TMP" '
	BEGIN { while((getline I < CHR_SIZE ) > 0) {split(I,J,/\t/); A[J[1]]=J[2] }}
	{
		if($6=="+")	X[$1]+=$3-$2
		else Y[$1]+=$3-$2
	}
	END {for(i in X) print i,X[i]*100/A[i],Y[i]*100/A[i] }
	' | sort -k1,1 > "${TMP}uniq.txt"

echo CHR uniq_+ uniq_- all_+ all_- | tr ' ' '\t' > "${PUBLIC}mappability_full_chromosome.txt"
join "${TMP}uniq.txt" "${TMP}all.txt" | tr ' ' '\t' >> "${PUBLIC}mappability_full_chromosome.txt"

#--------------------------------------------------------------------------------------------------
#calculate percent mapability on 1kb tiles
cat "$CHR_SIZE" | sort -k1,1 | \
	awk -v OFS="\t" '
		{ for(i=0;i<=$2;i+=1000) {
				if($2-i < 1000 && $2-i > 0 ) {
					print $1,i,$2,$1":"i"-"$2,0,"+"
					print $1,i,$2,$1":"i"-"$2,0,"-"
				} else {
				  print $1,i,i+1000,$1":"i"-"i+1000,0,"+"
          print $1,i,i+1000,$1":"i"-"i+1000,0,"-" 
				}
			}
		}	
	' > "${TMP}1kb-tiles.bed"
	
bedtools intersect -wao -s -a "${TMP}1kb-tiles.bed" -b "${FOLDER}analyzable_regions.bed" | \
	awk -v OFS="\t" '
		{ Z[$4]=$4
			if($6=="+") X[$4]+=$NF
			else Y[$4]+=$NF
		}
		END {for(i in Z) {
			split(i,B,/:|-/)
			C=B[3]-B[2]
			print i,X[i]*100/C,Y[i]*100/C,B[1],B[2]
			}
		}
	' | sort -k4,4 -k5,5n | cut -f 1-3 > "${TMP}all.txt"

bedtools intersect -wao -s -a "${TMP}1kb-tiles.bed" -b "${FOLDER}analyzable_regions_uniq_only.bed" | \
	awk -v OFS="\t" '
		{
			if($6=="+") X[$4]+=$NF
			else Y[$4]+=$NF
		}
		END {for(i in X) {
			split(i,B,/:|-/)
			C=B[3]-B[2]
			print i,X[i]*100/C,Y[i]*100/C,B[1],B[2]
			}
		}
	' | sort -k4,4 -k5,5n | cut -f 1-3 > "${TMP}uniq.txt"

echo tile uniq_+ uniq_- all_+ all_- | tr ' ' '\t' > "${PUBLIC}mappability_1kb-tiles.txt"
join "${TMP}uniq.txt" "${TMP}all.txt" | tr ' ' '\t' >> "${PUBLIC}mappability_1kb-tiles.txt"

TIME2=$(date "+%s")
echo time to calculate statistics $(( ( TIME2 - TIME ) / 60 )) min $(( ( TIME2 - TIME ) % 60 )) sec >> "${PUBLIC}time.log"
TIME="$TIME2"

echo total time $(( ( TIME2 - TIME_ORIG ) / 60 )) min $(( ( TIME2 - TIME_ORIG ) % 60 )) sec >> "${PUBLIC}time.log"


#wait if background zipping is not finished
wait

#--------------------------------------------------------------------------------------------------
#finishing

TIME2=$(date "+%s")
echo total time $(( ( TIME2 - TIME_ORIG ) / 60 )) min $(( ( TIME2 - TIME_ORIG ) % 60 )) sec >> "${PUBLIC}time.log"

echo "done"
exit


